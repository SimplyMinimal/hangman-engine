import random


class Hangman:
    __player_name = ""
    __random_word = ""
    __score = 0
    __single_letter_score_addition = 1
    __bonus_points_multiplier = 2
    __lives = 6
    __round = 1
    correctLettersUsed = ""  # Only Letters guessed correctly (ones that were in the random word)
    guessed_letters = ""  # All Letters guessed by the player

    def __init__(self, name=""):
        # Set up Hangman game on first run
        self.__random_word = self.__get_random_word()
        self.player_name = name

    def did_player_win(self):
        if self.__lives > 0:
            # We still have lives left
            # Now we check if we guessed all letters
            for letter in self.__random_word:
                if letter not in self.correctLettersUsed:
                    return False  # Player is missing a letter
            return True  # We still have lives and guessed all letters
        else:
            return False  # Player does not have lives left

    # Show current round
    @property  # Here we use a getter property
    def round(self):
        return self.__round

    @property
    def score(self):
        return self.__score

    @property
    def lives(self):
        return self.__lives

    # Returns a random word from the word list.txt file
    def __get_random_word(self):
        word_list = open('word list.txt', 'r').readlines()
        word = random.choice(word_list)
        return word.upper().strip()  # Convert to Uppercase for matching purposes, strip extra spaces

    @property
    def player_name(self):
        return self.__player_name

    @player_name.setter  # Here we set a setter property to make sure user does not pass in invalid names
    def player_name(self, name):
        if name is None or name.strip() == "":
            name = "Guest_" + str(random.randint(0, 9000))  # Default to Guest name
        self.__player_name = name

    # Check if guess letter exists in random word
    def guess_letter(self, letter):
        # Convert letter to uppercase.
        # This ensures all guessed A-Z letters (even lowercase) are visually shown as uppercase
        letter = letter.upper()

        # Add letter to list of guessed letters
        if letter not in self.guessed_letters:
            self.guessed_letters += letter

        # Did we guess a correct letter?
        if letter in self.__random_word:
            if letter not in self.correctLettersUsed:
                self.correctLettersUsed += letter
            return True
        return False

    def guess_word(self, word):
        # Convert word to uppercase and trim any extra spaces
        word = word.upper().strip()

        # Did we guess a correct word?
        if word == self.__random_word:
            # Player guessed correctly, go ahead and populate correct letters for consistency
            for letter in word:
                if letter not in self.guessed_letters:
                    self.guessed_letters += letter
                    self.correctLettersUsed += letter
            return True
        return False

    # Restart and reinitialize hangman game
    def restart(self):
        self.__round = 1
        self.__score = 0
        self.__lives = 6
        self.correctLettersUsed = ""
        self.guessed_letters = ""
        self.__random_word = self.__get_random_word()

    # Don't clear player's score, keep going on to next round
    def next_round(self):
        self.__round += 1
        self.__lives = 6
        self.correctLettersUsed = ""
        self.guessed_letters = ""
        self.__random_word = self.__get_random_word()

    # Ensure user only guesses a single letter at a time
    def length_is_one(self, letter):
        return len(letter.strip()) == 1

    # Returns true if player is attempting to guess word
    def length_eq_word(self, word):
        return len(word.strip()) == len(self.__random_word)

    # Return Underscores in place of letters, only returns _ with correctly guessed letters
    def get_guessed_letters_state(self):
        word = self.__random_word.strip()
        underscores = ""
        for letter in word:
            if letter in self.correctLettersUsed:
                underscores += letter.upper() + " "
            else:
                underscores += "_ "
        return underscores.rstrip()

    def end_game_show_word(self):
        self.__lives = 0
        return self.__random_word

    # Main Hangman Turns
    def play(self, p_input):
        if self.__lives > 0:  # Check to make sure we have __lives left
            p_input = p_input.upper().strip()
            if self.length_is_one(p_input) and p_input not in self.guessed_letters:
                #  Single Letter detected, begin guessing letter
                if self.guess_letter(p_input):
                    # Player guessed letter correctly
                    self.__score += self.__single_letter_score_addition
                else:
                    # Player guessed incorrectly
                    self.__lives -= self.__single_letter_score_addition
            elif self.length_eq_word(p_input):
                # Player is trying to guess word
                if self.guess_word(p_input):
                    # Player guessed word correctly, award bonus points
                    self.__score += self.__single_letter_score_addition * self.__bonus_points_multiplier
                else:
                    # Player guessed incorrectly
                    self.__lives -= self.__single_letter_score_addition
        else:  # No Lives Left
            # TODO: End Game
            pass