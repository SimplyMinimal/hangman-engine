#hangman game engine

##Requirments
Python 3

##Setup
In Terminal/Command Prompt:
`git clone https://bitbucket.org/SimplyMinimal/hangman-engine/`

##Run
```
cd hangman-engine
python test_run.py
```