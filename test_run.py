from GameEngine import Hangman

player_one = Hangman()
print("Player 1 [", player_one.player_name, "] ", "Begin Game!")

while True:
    if player_one.lives == 0:
        print("Word: ", player_one.end_game_show_word())
        print("Game Over! Restarting Game...")
        player_one.restart()

    if player_one.did_player_win():
        print("You won!")
        player_one.next_round()

    print(" ")  # Line Break

    print(player_one.get_guessed_letters_state())
    g = input("Guess:")
    r = player_one.play(g)
    if r:
        print("Yep!")
    else:
        print("Nope.")

    print("Score:", player_one.score, "Lives:", player_one.lives, "Round:", player_one.round)
    print(player_one.get_guessed_letters_state())
    print("Letters Guessed: ", " ".join(player_one.guessed_letters))
